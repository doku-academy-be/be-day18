package com.example.shop.services;

import com.example.shop.dto.UserRequest;
import com.example.shop.dto.UserResponse;
import com.example.shop.entities.Products;
import com.example.shop.entities.Users;
import com.example.shop.repositories.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    private UserRepository userRepository;
    private Users convertToEntity(UserRequest req) {
        return modelMapper.map(req, Users.class);
    }

    private UserResponse convertToDto(Users user) {
        return modelMapper.map(user, UserResponse.class);
    }


    public UserResponse createUser(UserRequest user) {
        Users usr = convertToEntity(user);
        Users create = userRepository.save(usr);
        return convertToDto(create);
    }

    public List<UserResponse> getUserList() {
        List<Users> usr = userRepository.findAll();
        if (!usr.isEmpty()) {
            return usr.stream()
                    .map(users -> modelMapper.map(users, UserResponse.class))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public UserResponse getUserById(Long id) {
        Users usr = userRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return convertToDto(usr);
    }

    public UserResponse updateUserById(Users user) {
        Optional<Users> userFound = userRepository.findById(user.getId());

        if (userFound.isPresent()) {
            Users userUpdate = userFound.get();
            userUpdate.setName(user.getName());
            userUpdate.setEmail(user.getEmail());
            Users usr = userRepository.save(user);
            return convertToDto(usr);
        } else {
            throw new RuntimeException("Data not Found");
        }
    }

    public String deleteUserById(Long id) {
        userRepository.deleteById(id);
        return "User "+ id +" deleted";
    }
}
