package com.example.shop.services;

import com.example.shop.dto.ProductRequest;
import com.example.shop.dto.ProductResponse;
import com.example.shop.dto.UserRequest;
import com.example.shop.dto.UserResponse;
import com.example.shop.entities.Products;
import com.example.shop.entities.Users;
import com.example.shop.repositories.ProductRepository;
import jakarta.persistence.EntityNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    private ProductRepository productRepository;
    private Products convertToEntity(ProductRequest req) {

        return modelMapper.map(req, Products.class);
    }

    private ProductResponse convertToDto(Products products) {
        return modelMapper.map(products, ProductResponse.class);
    }


    public ProductResponse createProduct(ProductRequest product) {
        Products prod = convertToEntity(product);
        Products create = productRepository.save(prod);
        return convertToDto(create);
    }

    public List<ProductResponse> getProductList() {
        List<Products> prod = productRepository.findAll();
        if (!prod.isEmpty()) {
            return prod.stream()
                    .map(products -> modelMapper.map(products, ProductResponse.class))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public ProductResponse getProductById(Long id) {
        Products prod = productRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return convertToDto(prod);
    }

    public ProductResponse updateProductById(Products product) {
        Optional<Products> productFound = productRepository.findById(product.getId());

        if (productFound.isPresent()) {
            Products productUpdate = productFound.get();
            productUpdate.setName(product.getName());
            productUpdate.setPrice(product.getPrice());
            productUpdate.setStock(product.getStock());
            Products update = productRepository.save(product);
            return convertToDto(update);
        } else {
            return null;
        }
    }

    public String deleteProductById(Long id) {
        productRepository.deleteById(id);
        return "User "+ id +" deleted";
    }
}
