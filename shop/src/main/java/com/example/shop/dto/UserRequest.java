package com.example.shop.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRequest {
    @JsonProperty(required = true)
    private String name;
    @JsonProperty(required = true)
    private String email;
}
