package com.example.shop.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductRequest {

    @JsonProperty(required = true)
    private String name;
    @JsonProperty(required = true)
    private double price;
    @JsonProperty(required = true)
    private int stock;
}
