package com.example.shop.controller;

import com.example.shop.dto.UserRequest;
import com.example.shop.dto.UserResponse;
import com.example.shop.entities.Users;
import com.example.shop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shop/v1/user")
public class UserController {
    @Autowired
    private UserService services;

    @GetMapping
    public ResponseEntity<List<UserResponse>> getAllUsers() {
        return ResponseEntity.ok(services.getUserList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> getUserById(@PathVariable Long id) {
        return ResponseEntity.ok().body(this.services.getUserById(id));
    }

    @PostMapping
    public ResponseEntity<UserResponse> addUser(@RequestBody UserRequest user) {
        return ResponseEntity.ok(this.services.createUser(user));
    }



    @PutMapping
    public ResponseEntity<UserResponse> updateUser(@RequestBody Users user) {
        return ResponseEntity.ok().body(this.services.updateUserById(user));
    }

    @DeleteMapping("/{id}")
    public HttpStatus deleteUser(@PathVariable Long id) {
        this.services.deleteUserById(id);
        return HttpStatus.OK;
    }
}
