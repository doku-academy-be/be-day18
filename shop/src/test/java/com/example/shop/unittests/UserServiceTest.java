package com.example.shop.unittests;

import com.example.shop.dto.UserRequest;
import com.example.shop.dto.UserResponse;
import com.example.shop.entities.Users;
import com.example.shop.repositories.UserRepository;
import com.example.shop.services.UserService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import static org.assertj.core.api.Assertions.assertThat;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    @Mock
    UserRepository userRepository;

    ModelMapper modelMapper = Mockito.spy(new ModelMapper());
    @InjectMocks
    UserService userService = Mockito.spy(new UserService());

    @BeforeEach
    void setup(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void givenValidRequest_whenCreatedNewUser_thenShouldBeCreated(){
        Users newUser = new Users(
                "syukron",
                "syukron@mail.com"
        );
        Mockito.when(userRepository.save(Mockito.any(Users.class))).thenReturn(newUser);

        UserRequest newUserRequest = modelMapper.map(newUser,UserRequest.class);
        UserResponse newUserResponse = userService.createUser(newUserRequest);

        assertThat(newUserResponse.getName()).isEqualTo(newUser.getName());
        assertThat(newUserResponse.getEmail()).isEqualTo(newUser.getEmail());
    }

    @Test
    public void givenAllUserResponse_whenFindAll(){
        Mockito.when(userRepository.findAll()).thenReturn(List.of(new Users(),new Users()));
        assertThat(userService.getUserList()).hasSize(2);
    }

    @Test
    public void givenOneUserResponse_whenFindById(){
        Users usr = new Users(
          1L,
          "syukron",
                "syukron@mail.com"
        );
        Mockito.when(userRepository.findById(usr.getId())).thenReturn(Optional.of(usr));
        UserResponse getUserResponse = userService.getUserById(usr.getId());
        assertThat(getUserResponse.getId()).isEqualTo(usr.getId());
    }
}
