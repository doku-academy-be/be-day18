package com.example.shop.unittests;

import com.example.shop.dto.ProductRequest;
import com.example.shop.dto.ProductResponse;
import com.example.shop.dto.UserResponse;
import com.example.shop.entities.Products;
import com.example.shop.entities.Users;
import com.example.shop.repositories.ProductRepository;
import com.example.shop.services.ProductService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import static org.assertj.core.api.Assertions.assertThat;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {
    @Mock
    ProductRepository productRepository;

    ModelMapper modelMapper = Mockito.spy(new ModelMapper());

    @InjectMocks
    ProductService productService = Mockito.spy(new ProductService());

    @BeforeEach
    void setup(){ MockitoAnnotations.openMocks(this);}

    @Test
    public void givenValidRequest_whenCreatedNewProduct_thenShouldBeCreated(){
        Products prod = new Products(
                "odol",
                8000,
                100
        );
        Mockito.when(productRepository.save(Mockito.any(Products.class))).thenReturn(prod);

        ProductRequest newProductReq = modelMapper.map(prod,ProductRequest.class);
        ProductResponse newProductRes = productService.createProduct(newProductReq);

        assertThat(newProductRes.getName()).isEqualTo(prod.getName());
        assertThat(newProductRes.getPrice()).isEqualTo(prod.getPrice());
        assertThat(newProductRes.getStock()).isEqualTo(prod.getStock());
    }
    @Test
    public void givenAllProductResponse_whenFindAll(){
        Mockito.when(productRepository.findAll()).thenReturn(List.of(new Products(),new Products()));
        assertThat(productService.getProductList()).hasSize(2);
    }
    @Test
    public void givenOneProductResponse_whenFindById(){
        Products prod = new Products();
        prod.setId(1L);
        prod.setName("odol");
        prod.setPrice(8000);
        prod.setStock(100);
        Mockito.when(productRepository.findById(prod.getId())).thenReturn(Optional.of(prod));
        ProductResponse getProductResponse = productService.getProductById(prod.getId());
        assertThat(getProductResponse.getId()).isEqualTo(prod.getId());
    }
}
